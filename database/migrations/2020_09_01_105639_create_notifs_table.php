<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotifsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifs', function (Blueprint $table) {
            $table->id();
            $table->string('src',3)->index(); //OPR-USR
            $table->integer('src_id')->index();
            $table->string('context',10)->index();
            $table->integer('context_id')->index();
            $table->string('action',16)->nullable();
            $table->string('link',24)->nullable();
            $table->string('label',36)->nullable();
            $table->datetime('ladate');
            $table->integer('rread')->index();
            $table->timestamps();
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifs');
    }
}
