<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDemandeopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('demandeops', function (Blueprint $table) {
            $table->id();
            $table->integer('demande_id')->index();
            $table->integer('predemande_id')->index();
            $table->integer('op_id')->index();
            $table->integer('seats');
            $table->string('status',10)->index();
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('demandeops');
    }
}
