<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->id();
            $table->integer('op_id')->index();
            $table->integer('demande_id')->index()->nullable();
            $table->string('flight_type',10)->index();
            $table->string('ac',16)->index();
            $table->string('ac_category',16)->index();
            $table->string('from_airport_code',24)->index();
            $table->string('to_airport_code',24)->index();
            $table->string('from_city',45)->index();
            $table->string('to_city',45)->index();
            $table->datetime('ladate')->index();
            $table->time('std');
            $table->time('sta');
            $table->string('status',6)->index();
            $table->integer('seats');
            $table->float('price_unit',8,2);
            $table->string('currency',3);
            $table->datetime('validity')->index();
            $table->string('model',1)->index();
            $table->float('variation',8,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
