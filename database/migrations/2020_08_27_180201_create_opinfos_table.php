<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpinfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opinfos', function (Blueprint $table) {
            $table->id();
            $table->integer('op_id')->unique()->index();
            $table->string('name',56);
            $table->string('year',4)->nullable();
            $table->string('email',36)->nullable();
            $table->string('adress',256)->nullable();
            $table->string('phone',16);
            $table->string('phone24',16)->nullable();
            $table->string('phoneceo',16)->nullable();
            $table->string('phonecustomer',16)->nullable();
            $table->string('country',12)->index();
            $table->string('city',12)->index();
            $table->string('zipcode',12)->index();
            $table->string('aoc',56);
            $table->string('aoc_country',12)->index();
            $table->string('quality_certif',56)->nullable();
            $table->string('safety_certif',56)->nullable();
            $table->float('insurance',12,2)->index()->nullable();
            $table->string('website',96)->nullable();
            $table->string('ceo',56)->nullable();
            $table->string('facebook',96)->nullable();
            $table->string('linkedin',96)->nullable();
            $table->string('instagram',56)->nullable();
            $table->string('twitter',96)->nullable();
            $table->string('logo',56)->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opinfos');
    }
}
