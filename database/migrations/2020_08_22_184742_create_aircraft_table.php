<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAircraftTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aircrafts', function (Blueprint $table) {
            $table->id();
            $table->string('ac',16)->unique()->index();
            $table->string('reg',16)->index();
            $table->string('type',12)->index();
            $table->string('category',12)->index();
            $table->string('manif_code',12)->index();
            $table->integer('seats');
            $table->string('range',12);
            $table->string('standup',3); //yes/no
            $table->float('speed', 8, 2);
            $table->float('intervalle', 8, 2);
            $table->float('cheight', 8, 2);
            $table->float('clength', 8, 2);
            $table->float('cwidth', 8, 2);
            $table->float('luggage_cap', 8, 2);
            $table->float('age', 8, 2);
            $table->float('nb_engines', 8, 2);
            $table->integer('op_id')->index();
            $table->string('engine1',24)->index();
            $table->integer('total_time1')->nullable()->default(null);
            $table->string('engine2',24)->nullable()->default(null);
            $table->integer('total_time2')->nullable()->default(null);
            $table->string('engine3',24)->nullable()->default(null);
            $table->integer('total_time3')->nullable()->default(null);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aircrafts');
    }
}
