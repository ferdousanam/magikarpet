<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crews', function (Blueprint $table) {
            $table->id();
            $table->string('code',12)->unique();
            $table->string('first',28);
            $table->string('last',28);
            $table->string('phone',16)->nullable();
            $table->string('email',24)->nullable();
            $table->string('position',16)->index();
            $table->string('image',56)->nullable();
            $table->integer('iscrew')->index();
            $table->integer('op_id')->index();
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crews');
    }
}
