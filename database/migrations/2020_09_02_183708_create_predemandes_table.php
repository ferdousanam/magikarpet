<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePredemandesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('predemandes', function (Blueprint $table) {
            $table->id();
            $table->integer('ur_id')->index();
            $table->string('flight_type',10)->index();
            $table->string('from_city',45)->index();
            $table->string('to_city',45)->index();
            $table->string('flexibility_distance',24);
            $table->datetime('ladate')->index();
            $table->string('depart_Time',24);
            $table->string('felxibility_time',24);
            $table->string('model',1)->index();
            $table->integer('seats');
            $table->datetime('validity')->index();
            $table->string('status',6)->index();
            $table->string('trip',1)->index();

            $table->datetime('return_date')->index()->nullable();
            $table->string('return_Time',24)->nullable();
            $table->string('return_flexibility_distance',24)->nullable();
            $table->string('return_felxibility_time',24)->nullable();
           

            $table->string('stopover1',45)->index()->nullable();
            $table->date('ladate1')->index()->nullable();
            $table->string('depart_Time1',24)->nullable();
            $table->string('stopover2',45)->index()->nullable();
            $table->date('ladate2')->index()->nullable();
            $table->string('depart_Time2',24)->nullable();
            $table->string('stopover3',45)->index()->nullable();
            $table->date('ladate3')->index()->nullable();
            $table->string('depart_Time3',24)->nullable();
            $table->string('stopover4',45)->index()->nullable();
            $table->date('ladate4')->index()->nullable();
            $table->string('depart_Time4',24)->nullable();
            $table->string('stopover5',45)->index()->nullable();
            $table->date('ladate5')->index()->nullable();
            $table->string('depart_Time5',24)->nullable();

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('predemandes');
    }
}
