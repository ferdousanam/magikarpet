<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserinfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userinfos', function (Blueprint $table) {
            $table->id();
            $table->integer('ur_id')->unique()->index();
            $table->date('ladate')->nullable();
            $table->string('phone',16);
            $table->string('adress',256);
            $table->string('zipcode',12);
            $table->string('country',12);
            $table->string('city',12);
            $table->string('nationality',12);
            $table->binary('logo')->nullable();
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('userinfos');
    }
}
