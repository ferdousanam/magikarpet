<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfferDemandesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_demandes', function (Blueprint $table) {
            $table->id();
            $table->integer('offer_id')->index();
            $table->integer('demande_id')->index();
            $table->integer('predemande_id')->index();
            $table->integer('seats');
            $table->integer('ur_id')->index();
            $table->datetime('ladate')->index();
            $table->string('status',6)->index(); 
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_demandes');
    }
}
