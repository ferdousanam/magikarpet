<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->id();
            $table->string('code',12)->unique()->index();
            $table->string('iso',12)->nullable();
            $table->string('country_code',12)->index();
            $table->string('name',50)->index();
            $table->string('timezone',36);
            $table->string('latitude',36)->index()->nullable();
            $table->string('longitude',36)->index()->nullable();
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
