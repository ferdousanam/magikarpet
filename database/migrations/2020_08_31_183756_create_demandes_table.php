<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDemandesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('demandes', function (Blueprint $table) {
            $table->id();
            $table->integer('ur_id')->index();
            $table->integer('predemande_id')->index();
            $table->string('flight_type',10)->index();
            $table->string('from_city',45)->index();
            $table->string('to_city',45)->index();
            $table->string('flexibility_distance',24);
            $table->datetime('ladate')->index();
            $table->string('depart_Time',24);
            $table->string('felxibility_time',24);
            $table->string('model',1)->index();
            $table->integer('seats');
            $table->datetime('validity')->index();
            $table->string('status',6)->index();
            
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('demandes');
    }
}
