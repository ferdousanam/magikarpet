<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;



Route::post('login', 'ApiController@login');
Route::post('register', 'ApiController@register');
Route::post('userbyrole','ApiController@getByFilter');

Route::group(['middleware' => 'auth.jwt'], function () {
    Route::get('logout', 'ApiController@logout');
    
});

/////Karpetmates
Route::post('karpetmatebyfilter','KarpetmateController@getByFilter');
Route::get('karpetmate','KarpetmateController@getAll');
Route::get('karpetmate/{id}','KarpetmateController@getById');
Route::post('karpetmate','KarpetmateController@addNew');
Route::put('karpetmate/{id}','KarpetmateController@update');
Route::delete('karpetmate/{id}','KarpetmateController@delete');

////Conversations

Route::post('createconversationifnotexist','ConversationController@createIfNotExist');
Route::post('updatechatnotif','ConversationController@massUpdateNotif');
Route::post('unreadchat','ConversationController@unReadChatNotifs');
Route::post('uservatar','ConversationController@getUserAvatar');
Route::post('opavatar','ConversationController@getOpAvatar');
Route::post('useronversation','ConversationController@UserConversation');
Route::post('opconversation','ConversationController@opConversation');
Route::post('groupconversation','ConversationController@groupConversation');
Route::get('conversation','ConversationController@getAll');
Route::get('conversation/{id}','ConversationController@getById');
Route::post('conversation','ConversationController@addNew');
Route::put('conversation/{id}','ConversationController@update');
Route::delete('conversation/{id}','ConversationController@delete');

////messages
Route::post('messagebyfilter','MessageController@getByFilter');
Route::get('message','MessageController@getAll');
Route::get('message/{id}','MessageController@getById');
Route::post('message','MessageController@addNew');
Route::put('message/{id}','MessageController@update');
Route::delete('message/{id}','MessageController@delete');

////Offer_Demande
Route::post('closeofferdemande','OfferDemandController@closeOfferDemande');
Route::post('offerdemandebyfilter','OfferDemandController@getByFilter');
Route::get('offerdemande','OfferDemandController@getAll');
Route::get('offerdemande/{id}','OfferDemandController@getById');
Route::post('offerdemande','OfferDemandController@addNew');
Route::put('offerdemande/{id}','OfferDemandController@update');
Route::delete('offerdemande/{id}','OfferDemandController@delete');
////offers
Route::post('offerbyfilter','OfferController@getByFilter');
Route::get('offer','OfferController@getAll');
Route::get('offer/{id}','OfferController@getById');
Route::post('offer','OfferController@addNew');
Route::put('offer/{id}','OfferController@update');
Route::delete('offer/{id}','OfferController@delete');
////Notif
Route::post('notifyopdemande','NotifController@NotifyOpDemande');
Route::post('notifbyfilter','NotifController@getByFilter');
Route::get('notif','NotifController@getAll');
Route::get('notif/{id}','NotifController@getById');
Route::post('notif','NotifController@addNew');
Route::put('notif/{id}','NotifController@update');
Route::delete('notif/{id}','NotifController@delete');

////Bases Op
Route::post('baseopbyfilter','BaseopController@getByFilter');
Route::get('baseop','BaseopController@getAll');
Route::get('baseop/{id}','BaseopController@getById');
Route::post('baseop','BaseopController@addNew');
Route::put('baseop/{id}','BaseopController@update');
Route::delete('baseop/{id}','BaseopController@delete');

////Demandes Op
Route::post('demandeopsent','DemandeopController@updateStatus');
Route::post('demandeopbyfilter','DemandeopController@getByFilter');
Route::get('demandeop','DemandeopController@getAll');
Route::get('demandeop/{id}','DemandeopController@getById');
Route::post('demandeop','DemandeopController@addNew');
Route::delete('demandeop/{id}','DemandeopController@delete');

////Demandes
Route::post('demandebyfilter','DemandeController@getByFilter');
Route::get('demande','DemandeController@getAll');
Route::get('demande/{id}','DemandeController@getById');
Route::post('demande','DemandeController@addNew');
Route::put('demande/{id}','DemandeController@update');
Route::delete('demande/{id}','DemandeController@delete');

////Pre Demandes
Route::post('predemandebyfilter','PredemandeController@getByFilter');
Route::get('predemande','PredemandeController@getAll');
Route::get('predemande/{id}','PredemandeController@getById');
Route::post('predemande','PredemandeController@addNew');
Route::put('predemande/{id}','PredemandeController@update');
Route::delete('predemande/{id}','PredemandeController@delete');

////Flights 

Route::post('lastpools','FlightController@lastPools');
Route::post('poolmembers','FlightController@poolMembers');
Route::post('seatsdeparture','FlightController@seatsDeparture');
Route::post('availabledepartures','FlightController@availableDeparture');
Route::post('opprogress','FlightController@opFlightProgress');
Route::post('urprogress','FlightController@userFlightProgress');
Route::post('flightByOffer','FlightController@flightByOffer');
Route::post('seatsbyflight','FlightController@seatsByFlight');
Route::post('totalseats','FlightController@totalSeats');
Route::post('flightsearch','FlightController@searchFlights');
Route::post('flightbyfilter','FlightController@getByFilter');
Route::get('flight','FlightController@getAll');
Route::get('flight/{id}','FlightController@getById');
Route::post('flight','FlightController@addNew');
Route::put('flight/{id}','FlightController@update');
Route::delete('flight/{id}','FlightController@delete');
Route::get('timebyzone','FlightController@timeByZone');
Route::get('timenow','FlightController@timeNow');

////Flight Members
Route::post('update','FlightMemberController@confirmFlight');
Route::post('memberbyfilter','FlightMemberController@getByFilter');
Route::get('flightmember','FlightMemberController@getAll');
Route::get('flightmember/{id}','FlightMemberController@getById');
Route::post('flightmember','FlightMemberController@addNew');
Route::put('flightmember/{id}','FlightMemberController@update');
Route::delete('flightmember/{id}','FlightMemberController@delete');

////Cities
Route::post('citybycode','CityController@getByCode');
Route::post('citybyfilter','CityController@getByFilter');
Route::get('city','CityController@getAll');
Route::get('city/{id}','CityController@getById');
Route::post('city','CityController@addNew');
Route::put('city/{id}','CityController@update');
Route::delete('city/{id}','CityController@delete');

////Countries
Route::post('countrybycode','CountryController@getByCode');
Route::get('country','CountryController@getAll');
Route::get('country/{id}','CountryController@getById');
Route::post('country','CountryController@addNew');
Route::put('country/{id}','CountryController@update');
Route::delete('country/{id}','CountryController@delete');

////Airports
Route::post('airportcity','AirportController@getAirportsCity');
Route::post('airportbyfilter','AirportController@getByFilter');
Route::get('airport','AirportController@getAll');
Route::get('airport/{id}','AirportController@getById');
Route::post('airport','AirportController@addNew');
Route::put('airport/{id}','AirportController@update');
Route::delete('airport/{id}','AirportController@delete');

////Aircrafts
Route::post('aircraftbyfilter','AircraftController@getByFilter');
Route::get('aircraft','AircraftController@getAll');
Route::get('aircraft/{id}','AircraftController@getById');
Route::post('aircraft','AircraftController@addNew');
Route::put('aircraft/{id}','AircraftController@update');
Route::delete('aircraft/{id}','AircraftController@delete');

////Operators Info
Route::post('operatorbyfilter','OpinfoController@getByFilter');
Route::get('operator','OpinfoController@getAll');
Route::get('operator/{id}','OpinfoController@getById');
Route::post('operator','OpinfoController@addNew');
Route::put('operator/{id}','OpinfoController@update');
Route::delete('operator/{id}','OpinfoController@delete');

////Users Info

Route::post('usersearch','UserinfoController@userSearch');
Route::get('usergeneralinfo','UserinfoController@userInfo');
Route::post('userbyfilter','UserinfoController@getByFilter');
Route::get('user','UserinfoController@getAll');
Route::get('user/{id}','UserinfoController@getById');
Route::post('user','UserinfoController@addNew');
Route::put('user/{id}','UserinfoController@update');
Route::delete('user/{id}','UserinfoController@delete');

////Gallery
Route::post('upload','GalleryController@upload');
Route::get('download','GalleryController@download');
Route::post('gallerybyfilter','GalleryController@getByFilter');
Route::get('gallery','GalleryController@getAll');
Route::get('gallery/{id}','GalleryController@getById');
Route::post('gallery','GalleryController@addNew');
Route::post('gallerymany','GalleryController@addMany');
Route::put('gallery/{id}','GalleryController@update');
Route::delete('gallery/{id}','GalleryController@delete');

////Manifacturer
Route::get('manifacturer','ManifacturerController@getAll');
Route::get('manifacturer/{id}','ManifacturerController@getById');
Route::post('manifacturer','ManifacturerController@addNew');
Route::put('manifacturer/{id}','ManifacturerController@update');
Route::delete('manifacturer/{id}','ManifacturerController@delete');

////Crew & Staff
Route::post('crewbyfilter','CrewController@getByFilter');
Route::get('crew','CrewController@getAll');
Route::get('crew/{id}','CrewController@getById');
Route::post('crew','CrewController@addNew');
Route::put('crew/{id}','CrewController@update');
Route::delete('crew/{id}','CrewController@delete');

