<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notif extends Model
{
    protected $table = "notifs";
    public $fillable = ['src','src_id','label','context','context_id','action','link','ladate','rread'];
}
