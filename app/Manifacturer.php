<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manifacturer extends Model
{
    protected $table = "manifacturers";
    public $fillable = ['code','name'];
}
