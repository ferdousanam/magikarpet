<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Userinfo;
use DB;
use Illuminate\Support\Str;

class UserinfoController extends Controller
{
    
    public function getByFilter(Request $request){
       
        $userinfo =  Userinfo::where('ur_id', '=', $request->ur_id)->get(); 
            if (is_null($userinfo)){
                return response()->json("Record Not Found",404); 
            } else{
                return response()->json($userinfo,200);
            }
    }

    public function userInfo(){
       
            $userinfo =  DB::table('userinfos')
                            ->select('id','ur_id','ladate','phone','adress','zipcode', 'country','city','nationality', 'logo')
                            ->get();

            if (is_null($userinfo)){
                return response()->json("Record Not Found",404); 
            } else{
                return response()->json($userinfo,200);
            }
    }

    public function userSearch(Request $request){
        $search = $request->search;
        $from = $request->from;
        $to = $request->to;
        $limit = $request->limit;
        
        if ($from){

            $userinfo =   DB::table('flights')->join('flight_members', 'flights.id', '=', 'flight_members.id_flight')
            ->join("userinfos",function($join){
                $join->on("flight_members.id_client","=","userinfos.ur_id");
                
            })->join("users",function($join){
                $join->on("users.id","=","userinfos.ur_id");
            })
            ->where(function ($query) use ($from) {
                $query->where('from_airport_code', '=', $from)->orWhere('from_city', '=', $from);
            })->where(function($query) use ($to) {
                $query->where('to_airport_code', '=', $to)->orWhere('to_city', '=', $to);
            })->where(function ($query) use ($search) {	
                $query->where('last', 'like', '%' . $search . '%')
                ->orWhere('first', 'like', '%' . $search . '%')
                ->orWhere('email', 'like', '%' . $search . '%')
                ->orWhere('phone', 'like', '%' . $search . '%');
            })
            ->where('flights.status','=','CLO')
            ->select('ur_id','users.first','users.last','users.email','phone','adress','zipcode', 'country','city','nationality', 'logo') 
            ->distinct()
            ->limit($limit)
            ->get();

        } else{

            $userinfo = DB::table('users')->join('userinfos', 'users.id', '=', 'userinfos.ur_id')
            ->select('first','last','email','ur_id','ladate','phone','adress','zipcode', 'country','city','nationality', 'logo')
            ->where('last', 'like', '%' . $search . '%')
            ->orWhere('first', 'like', '%' . $search . '%')
            ->orWhere('email', 'like', '%' . $search . '%')
            ->orWhere('phone', 'like', '%' . $search . '%')
            ->limit($limit)
            ->get();
        }
       

    
           
            
            

            if (is_null($userinfo)){
                return response()->json("Record Not Found",404); 
            } else{
                return response()->json($userinfo,200);
            }
    }

    public function getAll(){
        return response()->json(Userinfo::get(),200);
    }

    public function getById($id){
        $userinfo = Userinfo::find($id);
        if (is_null($userinfo)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($userinfo,200);
        }
       
    }

    public function addNew(Request $request){
        $userinfo = Userinfo::create($request->all());
        return response()->json($userinfo,201);
    }

    public function update($id,Request $request){
        $userinfo = Userinfo::find($id);
        if (is_null($userinfo)){
            return response()->json("Record Not Found",404); 
        } else{
            $userinfo->update($request->all());
            return response()->json($userinfo,200);
        }

        
    }

    public function delete($id){
        $userinfo = Userinfo::find($id);
        if (is_null($userinfo)){
            return response()->json("Record Not Found",404); 
        } else{
            $userinfo->delete();
            return response()->json(null,204);  
        }
         
    }




}
