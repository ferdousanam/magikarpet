<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use \Carbon\Carbon;
use App\OfferDemande;
use App\Offer;
use DB;



class OfferDemandController extends Controller
{
    public function closeOfferDemande(Request $request){
        $demande_id = $request->demande_id;
       
        $affected = DB::table('offer_demandes')
              ->where('demande_id', $demande_id)
              ->update(['status' => 'CLO']);
        
        if (is_null($affected)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($affected,200);
        }

    }

    public function getByFilter(Request $request){
       
        $offerdemande =  DB::table('offers')
                        ->join('offer_demandes', 'offers.id', '=', 'offer_demandes.offer_id')
                        ->join("opinfos",function($join){
                            $join->on("offers.op_id","=","opinfos.op_id");
                        })
                        ->select('offer_demandes.id as id','offer_demandes.offer_id','offer_demandes.predemande_id as predemande_id','offer_demandes.demande_id','offers.op_id','ur_id','flight_type','ac',
                                'ac_category','from_city','to_city','offers.ladate as ladate','std','sta','offer_demandes.status','offers.seats as seats','offer_demandes.seats as requestedSeats',
                                'price_unit','currency','validity','model','variation','from_airport_code','to_airport_code','opinfos.logo')
                        ->where('ur_id', '=', $request->ur_id)
                        ->where('offer_demandes.status', '=', $request->status)
                        ->orderBy('offer_id','desc')
                        ->get();
        if (is_null($offerdemande)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($offerdemande,200);
        }

    }
    
    public function getAll(){
        return response()->json(OfferDemande::get(),200);
    }

    public function getById($id){
        $offerdemande = OfferDemande::find($id);
        if (is_null($offerdemande)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($offerdemande,200);
        }
       
    }

    public function addNew(Request $request){
        
        if( $request->ur_id1 > ''){
            $data = array(
                'offer_id'=> $request->offer_id,
                'demande_id'=> $request->demande_id1,
                'predemande_id'=> $request->predemande_id1, 
                'ur_id'=> $request->ur_id1,
                'seats'=> $request->seats1,
                'status'=>'OPEN',
                'ladate'=>Carbon::now()->format('Y-m-d H:i:s'),
            );
            DB::table('offer_demandes')->insert($data);
            //insert notif for User
            $data2 = array(
                'src'=> 'USR', 
                'src_id'=> $request->ur_id1,
                'context'=> 'OFFER', 
                'context_id'=> $request->offer_id,
                'label'=>'New Flight Offer',
                'ladate'=>Carbon::now()->format('Y-m-d H:i:s'),
                'rread'=>0,
            );
            
            DB::table('notifs')->insert($data2);
        }

        if( $request->ur_id2 > ''){
            $data = array(
                'offer_id'=> $request->offer_id,
                'demande_id'=> $request->demande_id2, 
                'predemande_id'=> $request->predemande_id2, 
                'ur_id'=> $request->ur_id2,
                'seats'=> $request->seats2,
                'status'=>'OPEN',
                'ladate'=>Carbon::now()->format('Y-m-d H:i:s'),
            );
            DB::table('offer_demandes')->insert($data);
             //insert notif for User
             $data2 = array(
                'src'=> 'USR', 
                'src_id'=> $request->ur_id2,
                'context'=> 'OFFER', 
                'context_id'=>$request->offer_id,
                'label'=>'New Flight Offer',
                'ladate'=>Carbon::now()->format('Y-m-d H:i:s'),
                'rread'=>0,
            );
            
            DB::table('notifs')->insert($data2);
        }

        if( $request->ur_id3 > ''){
            $data = array(
                'offer_id'=> $request->offer_id,
                'demande_id'=> $request->demande_id3, 
                'predemande_id'=> $request->predemande_id3, 
                'ur_id'=> $request->ur_id3,
                'seats'=> $request->seats3,
                'status'=>'OPEN',
                'ladate'=>Carbon::now()->format('Y-m-d H:i:s'),
            
            );
            DB::table('offer_demandes')->insert($data);
             //insert notif for User
             $data2 = array(
                'src'=> 'USR', 
                'src_id'=> $request->ur_id3,
                'context'=> 'OFFER', 
                'context_id'=> $request->offer_id,
                'label'=>'New Flight Offer',
                'ladate'=>Carbon::now()->format('Y-m-d H:i:s'),
                'rread'=>0,
            );
            
            DB::table('notifs')->insert($data2);
        }

        if( $request->ur_id4 > ''){
            $data = array(
                'offer_id'=> $request->offer_id,
                'demande_id'=> $request->demande_id4, 
                'predemande_id'=> $request->predemande_id4, 
                'ur_id'=> $request->ur_id4,
                'seats'=> $request->seats4,
                'status'=>'OPEN',
                'ladate'=>Carbon::now()->format('Y-m-d H:i:s'),
            );
            DB::table('offer_demandes')->insert($data);
             //insert notif for User
             $data2 = array(
                'src'=> 'USR', 
                'src_id'=> $request->ur_id4,
                'context'=> 'OFFER', 
                'context_id'=>$request->offer_id,
                'label'=>'New Flight Offer',
                'ladate'=>Carbon::now()->format('Y-m-d H:i:s'),
                'rread'=>0,
            );
            
            DB::table('notifs')->insert($data2);
        }

        if( $request->ur_id5 > ''){
            $data = array(
                'offer_id'=> $request->offer_id,
                'demande_id'=> $request->demande_id5, 
                'predemande_id'=> $request->predemande_id5, 
                'ur_id'=> $request->ur_id5,
                'seats'=> $request->seats5,
                'status'=>'OPEN',
                'ladate'=>Carbon::now()->format('Y-m-d H:i:s'),
            );
            DB::table('offer_demandes')->insert($data);
            //insert notif for User
            $data2 = array(
                'src'=> 'USR', 
                'src_id'=> $request->ur_id5,
                'context'=> 'OFFER', 
                'context_id'=>$request->offer_id,
                'label'=>'New Flight Offer',
                'ladate'=>Carbon::now()->format('Y-m-d H:i:s'),
                'rread'=>0,
            );
        }

        return response()->json('OK',201);
    }

    public function update($id,Request $request){
        $offerdemande = OfferDemande::find($id);
        if (is_null($offerdemande)){
            return response()->json("Record Not Found",404); 
        } else{
            $offerdemande->update($request->all());
            return response()->json($offerdemande,200);
        }

        
    }

    public function delete($id){
        $offerdemande = OfferDemande::find($id);
        if (is_null($offerdemande)){
            return response()->json("Record Not Found",404); 
        } else{
            $offerdemande->delete();
            return response()->json(null,204);  
        }
         
    }



}
