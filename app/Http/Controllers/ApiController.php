<?php

namespace App\Http\Controllers;

use JWTAuth;
use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Str;
use DB;

class ApiController extends Controller
{
    /**
     * @var bool
     */
    public $loginAfterSignUp = false;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
public function getByFilter(Request $request){
    
    $users = User::where('role', '=', $request->role)->get();;
        if (is_null($users)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($users,200);
        }
}
public function login(Request $request)
    {
        $input = $request->only('email', 'password');
        $token = null;

        if (!$token = JWTAuth::attempt($input)) {
            
            return response()->json([
                'success' => false,
                'message' => 'Invalid username or Password',
            ], 401);
        }

        $user =  User::where('email', '=', $request->email)->get();
        return response()->json([
            'success' => true,
            'token' => $token,
            'user' => $user,
        ]);
    }

    public function refreshToken(){
       $token = JWTAuth::getToken();

        if (! $token) {
            throw new BadRequestHttpException('Token not provided');
        }

        try {
            $token = JWTAuth::refresh($token);
        } catch (TokenInvalidException $e) {
            throw new AccessDeniedHttpException('The token is invalid');
        }

        return response()->json(compact('token'));
    }


    //generate password reset token
    public function genResetLink(Request $request){
        $username = $request->username;
        DB::table('password_resets')->where('email', '=', $username)->delete(); //dlelet if exist
        
        $reset_token = Str::random(56);
        DB::table('password_resets')->insert(
            ['email' => $username, 'token' => $reset_token]
        );

        return response()->json([
            'success' => true,
            'resetToken' => $reset_token
        ]);

    }

    
    public function resetPassword(Request $request){
       
        $TokenExist =  DB::table('password_resets')->where('email', '=', $request->email)->where('token', '=', $request->token)->get();

        
        if (count($TokenExist)<1){
            return response()->json("Invalid Token",404); 
        } else{
            ;
            $data = array(
                'password'=>bcrypt($request->password),  
            );
            
            DB::table('users')
            ->where('email', $request->email)
            ->update($data);
            return response()->json($data,200);
        }
    }

   
    public function logout(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);

        try {
            JWTAuth::invalidate($request->token);

            return response()->json([
                'success' => true,
                'message' => 'User logged out successfully'
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, the user cannot be logged out'
            ], 500);
        }
    }

    /**
     * @param RegistrationFormRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $user = new User();
        $user->title = $request->title;
        $user->first = $request->first;
        $user->last = $request->last;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->status = $request->status;
        $user->password = bcrypt($request->password);
        $user->save();

        return response()->json($user,200);

        // if ($this->loginAfterSignUp) {
        //     return $this->login($request);
        // }
      
       

    //     return response()->json([
    //         'success'   =>  true,
    //         'data'      =>  $user
    //     ], 200);
    
    }



    public function getAll(){
        return response()->json(User::get(),200);
    }

    public function getById($id){
        $user = User::find($id);
        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'User cannot be found.'
            ], 400);
        }

        return response()->json($user,200);
       
    }

    public function update($id,Request $request){
        $user = User::find($id);
        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'User cannot be found.'
            ], 400);
        }
        
        $user->update($request->all());
        return response()->json($user,200);

        

        
    }


    public function delete($id){
        $user = User::find($id);
        if (is_null($user)){
            return response()->json("Record Not Found",404); 
        } else{
            $user->delete();
            return response()->json(null,204);  
        }
         
    }
}