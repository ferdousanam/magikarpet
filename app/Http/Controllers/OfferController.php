<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Offer;

class OfferController extends Controller
{
    
    public function getByFilter(Request $request){
       
        $offer =  Offer::where('op_id', '=', $request->op_id)
                        ->where('status', '=', $request->status)
                        ->orderBy('id','desc')
                        ->get(); // crew = 1, staff = 0
            if (is_null($offer)){
                return response()->json("Record Not Found",404); 
            } else{
                return response()->json($offer,200);
            }
    }

    public function getAll(){
        return response()->json(Offer::get(),200);
    }

    public function getById($id){
        $offer = Offer::find($id);
        if (is_null($offer)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($offer,200);
        }
       
    }

    public function addNew(Request $request){
        $offer = Offer::create($request->all());
        return response()->json($offer,201);
    }

    public function update($id,Request $request){
        $offer = Offer::find($id);
        if (is_null($offer)){
            return response()->json("Record Not Found",404); 
        } else{
            $offer->update($request->all());
            return response()->json($offer,200);
        }

        
    }

    public function delete($id){
        $offer = Offer::find($id);
        if (is_null($offer)){
            return response()->json("Record Not Found",404); 
        } else{
            $offer->delete();
            return response()->json(null,204);  
        }
         
    }


}
