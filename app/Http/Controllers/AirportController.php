<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Airport;
use DB;

class AirportController extends Controller
{



    public function getByFilter(Request $request){

        $airport = DB::table('airports')->join('cities', 'airports.city_code', '=', 'cities.code')
        ->select('airports.code as code','airports.code as code2','airports.city_code','cities.timezone','airports.name as name_ar', 'cities.name as name_ct','cities.latitude','cities.longitude')
        ->get();

        if (is_null($airport)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($airport,200);
        }

    }

    public function getAirportsCity(Request $request){
        $airport =  Airport::where('city_code', '=', $request->city_code)->get(); // crew = 1, staff = 0
        if (is_null($airport)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($airport,200);
        }
    }


    public function getAll(){
        return response()->json(Airport::get(),200);
    }

    public function getById($id){
        $airport = Airport::find($id);
        if (is_null($airport)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($airport,200);
        }
       
    }

    public function addNew(Request $request){
        $airport = Airport::create($request->all());
        return response()->json($airport,201);
    }

    public function update($id,Request $request){
        $airport = Airport::find($id);
        if (is_null($airport)){
            return response()->json("Record Not Found",404); 
        } else{
            $airport->update($request->all());
            return response()->json($airport,200);
        }

        
    }

    public function delete($id){
        $airport = Airport::find($id);
        if (is_null($airport)){
            return response()->json("Record Not Found",404); 
        } else{
            $airport->delete();
            return response()->json(null,204);  
        }
         
    }


}
