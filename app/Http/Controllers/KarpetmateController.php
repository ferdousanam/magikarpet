<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Karpetmate;

class KarpetmateController extends Controller
{

    public function getByFilter(Request $request){
       
        $karpetmate =  Karpetmate::where('ur_id1', '=', $request->ur_id)
                            ->orWhere('ur_id2', '=', $request->ur_id)
                            ->get(); 
            if (is_null($karpetmate)){
                return response()->json("Record Not Found",404); 
            } else{
                return response()->json($karpetmate,200);
            }
    }

    

    public function getAll(){
        return response()->json(Karpetmate::get(),200);
    }

    public function getById($id){
        $karpetmate = Karpetmate::find($id);
        if (is_null($karpetmate)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($karpetmate,200);
        }
       
    }

    public function addNew(Request $request){
        $karpetmate = Karpetmate::create($request->all());
        return response()->json($karpetmate,201);
    }

    public function update($id,Request $request){
        $karpetmate = Karpetmate::find($id);
        if (is_null($karpetmate)){
            return response()->json("Record Not Found",404); 
        } else{
            $karpetmate->update($request->all());
            return response()->json($karpetmate,200);
        }

        
    }

    public function delete($id){
        $karpetmate = Karpetmate::find($id);
        if (is_null($karpetmate)){
            return response()->json("Record Not Found",404); 
        } else{
            $karpetmate->delete();
            return response()->json(null,204);  
        }
         
    }


    
}
