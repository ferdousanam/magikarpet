<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Aircraft;

class AircraftController extends Controller
{

    public function getByFilter(Request $request){
       
        $aircraft =  Aircraft::where('op_id', '=', $request->op_id)->get(); // crew = 1, staff = 0
            if (is_null($aircraft)){
                return response()->json("Record Not Found",404); 
            } else{
                return response()->json($aircraft,200);
            }
    }

    public function getAll(){
        return response()->json(Aircraft::get(),200);
    }

    public function getById($id){
        $aircraft = Aircraft::find($id);
        if (is_null($aircraft)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($aircraft,200);
        }
       
    }

    public function addNew(Request $request){
        $aircraft = Aircraft::create($request->all());
        return response()->json($aircraft,201);
    }

    public function update($id,Request $request){
        $aircraft = Aircraft::find($id);
        if (is_null($aircraft)){
            return response()->json("Record Not Found",404); 
        } else{
            $aircraft->update($request->all());
            return response()->json($aircraft,200);
        }

        
    }

    public function delete($id){
        $aircraft = Aircraft::find($id);
        if (is_null($aircraft)){
            return response()->json("Record Not Found",404); 
        } else{
            $aircraft->delete();
            return response()->json(null,204);  
        }
         
    }


}
