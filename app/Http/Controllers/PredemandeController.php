<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Predemande;
use App\Demande;
Use \Carbon\Carbon;
use App\Notif;
use App\Baseop;
use App\Demandeop;
use Illuminate\Support\Str;
use DB;

class PredemandeController extends Controller
{
    public function genOpDemNotif($dem,$pre,$cit,$seat){

        $baseops =  Baseop::select('op_id')->distinct()->where('airport_code', '=', $cit)
                            ->orWhere('city_code', '=', $cit)
                            ->get();

            if (is_null($baseops)){
                
            } else{
                

                foreach($baseops as $baseop){
                    //insert demande for OP
                    $data = array(
                        'predemande_id'=> $pre,
                        'demande_id'=> $dem,
                        'seats'=> $seat, 
                        'op_id'=> $baseop->op_id,
                        'status'=>'OPEN',
                    
                    );
                    
                    DB::table('demandeops')->insert($data);

                    //insert notif for OP
                    $data2 = array(
                        'src'=> 'OPR', 
                        'context'=> 'DEMANDE', 
                        'context_id'=> $dem, 
                        'src_id'=> $baseop->op_id,
                        'label'=>'New Flight Request',
                        'ladate'=>Carbon::now()->format('Y-m-d H:i:s'),
                        'rread'=>0,
                    );
                    
                    DB::table('notifs')->insert($data2);

                }// end foreach
            
            }//end else


    }

    public function addNew(Request $request){
        $predemande = Predemande::create($request->all());

        $predemande_id = $predemande->id;
        $trip = $predemande->trip;

        if($trip==1){
            $data = array(
                'predemande_id'=> $predemande->id,
                'ur_id'=> $predemande->ur_id,
                'flight_type'=> $predemande->flight_type,
                'from_city'=> $predemande->from_city,
                'to_city'=> $predemande->to_city,
                'flexibility_distance'=> $predemande->flexibility_distance,
                'ladate'=> $predemande->ladate,
                'depart_Time'=> $predemande->depart_Time,
                'felxibility_time'=> $predemande->felxibility_time,
                'model'=> $predemande->model,
                'seats'=> $predemande->seats, 
                'validity'=> $predemande->validity,
                'status'=> $predemande->status,
            
            );
            DB::table('demandes')->insert($data);

            $demande_id = DB::getPdo()->lastInsertId();
            $this->genOpDemNotif($demande_id, $predemande_id,$predemande->from_city,$predemande->seats);

        }

        if($trip==2){ //round trip
            $data = array( //aller
                'predemande_id'=> $predemande->id,
                'ur_id'=> $predemande->ur_id,
                'flight_type'=> $predemande->flight_type,
                'from_city'=> $predemande->from_city,
                'to_city'=> $predemande->to_city,
                'flexibility_distance'=> $predemande->flexibility_distance,
                'ladate'=> $predemande->ladate,
                'depart_Time'=> $predemande->depart_Time,
                'felxibility_time'=> $predemande->felxibility_time,
                'model'=> $predemande->model,
                'seats'=> $predemande->seats, 
                'validity'=> $predemande->validity,
                'status'=> $predemande->status,
            
            );
            DB::table('demandes')->insert($data);

            $demande_id = DB::getPdo()->lastInsertId();
            $this->genOpDemNotif($demande_id, $predemande_id,$predemande->from_city,$predemande->seats);

            $data = array( //retour
                'predemande_id'=> $predemande->id,
                'ur_id'=> $predemande->ur_id,
                'flight_type'=> $predemande->flight_type,
                'from_city'=> $predemande->to_city,
                'to_city'=> $predemande->from_city,
                'flexibility_distance'=> $predemande->return_flexibility_distance,
                'ladate'=> $predemande->return_date,
                'depart_Time'=> $predemande->return_Time,
                'felxibility_time'=> $predemande->return_felxibility_time,
                'model'=> $predemande->model,
                'seats'=> $predemande->seats, 
                'validity'=> $predemande->validity,
                'status'=> $predemande->status,
            
            );
            DB::table('demandes')->insert($data);

            $demande_id = DB::getPdo()->lastInsertId();
            $this->genOpDemNotif($demande_id, $predemande_id,$predemande->to_city,$predemande->seats);
           
        }

        if($trip==3){//multi city
            if(Str::length($predemande->stopover1)>0){
                $data = array( //step 1
                    'predemande_id'=> $predemande->id,
                    'ur_id'=> $predemande->ur_id,
                    'flight_type'=> $predemande->flight_type,
                    'from_city'=> $predemande->from_city,
                    'to_city'=> $predemande->stopover1,
                    'flexibility_distance'=> $predemande->flexibility_distance,
                    'ladate'=> $predemande->ladate1,
                    'depart_Time'=> $predemande->depart_Time1,
                    'felxibility_time'=> $predemande->felxibility_time,
                    'model'=> $predemande->model,
                    'seats'=> $predemande->seats, 
                    'validity'=> $predemande->validity,
                    'status'=> $predemande->status,
                
                );
                DB::table('demandes')->insert($data);

                $demande_id = DB::getPdo()->lastInsertId();
                $this->genOpDemNotif($demande_id, $predemande_id,$predemande->from_city,$predemande->seats);
            }
            
            
            if(Str::length($predemande->stopover2)>0){
                $data = array( //step 1
                    'predemande_id'=> $predemande->id,
                    'ur_id'=> $predemande->ur_id,
                    'flight_type'=> $predemande->flight_type,
                    'from_city'=> $predemande->stopover1,
                    'to_city'=> $predemande->stopover2,
                    'flexibility_distance'=> "NONE",
                    'ladate'=> $predemande->ladate2,
                    'depart_Time'=> $predemande->depart_Time2,
                    'felxibility_time'=> "NONE",
                    'model'=> $predemande->model,
                    'seats'=> $predemande->seats, 
                    'validity'=> $predemande->validity,
                    'status'=> $predemande->status,
                
                );
                DB::table('demandes')->insert($data);

                $demande_id = DB::getPdo()->lastInsertId();
                $this->genOpDemNotif($demande_id, $predemande_id,$predemande->stopover1,$predemande->seats);
            }

            if(Str::length($predemande->stopover3)>0){
                $data = array( //step 1
                    'predemande_id'=> $predemande->id,
                    'ur_id'=> $predemande->ur_id,
                    'flight_type'=> $predemande->flight_type,
                    'from_city'=> $predemande->stopover2,
                    'to_city'=> $predemande->stopover3,
                    'flexibility_distance'=> "NONE",
                    'ladate'=> $predemande->ladate3,
                    'depart_Time'=> $predemande->depart_Time3,
                    'felxibility_time'=> "NONE",
                    'model'=> $predemande->model,
                    'seats'=> $predemande->seats, 
                    'validity'=> $predemande->validity,
                    'status'=> $predemande->status,
                
                );
                DB::table('demandes')->insert($data);

                $demande_id = DB::getPdo()->lastInsertId();
                $this->genOpDemNotif($demande_id, $predemande_id,$predemande->stopover2,$predemande->seats);                
            }

            if(Str::length($predemande->stopover4)>0){
                $data = array( //step 1
                    'predemande_id'=> $predemande->id,
                    'ur_id'=> $predemande->ur_id,
                    'flight_type'=> $predemande->flight_type,
                    'from_city'=> $predemande->stopover3,
                    'to_city'=> $predemande->stopover4,
                    'flexibility_distance'=> "NONE",
                    'ladate'=> $predemande->ladate4,
                    'depart_Time'=> $predemande->depart_Time4,
                    'felxibility_time'=> "NONE",
                    'model'=> $predemande->model,
                    'seats'=> $predemande->seats, 
                    'validity'=> $predemande->validity,
                    'status'=> $predemande->status,
                
                );
                DB::table('demandes')->insert($data);

                $demande_id = DB::getPdo()->lastInsertId();
                $this->genOpDemNotif($demande_id, $predemande_id,$predemande->stopover3,$predemande->seats);
            }

            if(Str::length($predemande->stopover5)>0){
                $data = array( //step 1
                    'predemande_id'=> $predemande->id,
                    'ur_id'=> $predemande->ur_id,
                    'flight_type'=> $predemande->flight_type,
                    'from_city'=> $predemande->stopover4,
                    'to_city'=> $predemande->stopover5,
                    'flexibility_distance'=> "NONE",
                    'ladate'=> $predemande->ladate5,
                    'depart_Time'=> $predemande->depart_Time5,
                    'felxibility_time'=> "NONE",
                    'model'=> $predemande->model,
                    'seats'=> $predemande->seats, 
                    'validity'=> $predemande->validity,
                    'status'=> $predemande->status,
                
                );
                DB::table('demandes')->insert($data);

                $demande_id = DB::getPdo()->lastInsertId();
                $this->genOpDemNotif($demande_id, $predemande_id,$predemande->stopover4,$predemande->seats);
            }
           

        }

        return response()->json($predemande,200);

    }

    public function getByFilter(Request $request){
       
        $predemande =  Predemande::where('ur_id', '=', $request->ur_id)
                                 ->where('status', '=', $request->status)
                                 ->orderBy('id','desc')
                                 ->get(); // crew = 1, staff = 0
            if (is_null($predemande)){
                return response()->json("Record Not Found",404); 
            } else{
                return response()->json($predemande,200);
            }
    }
    


    public function getAll(){
        return response()->json(Predemande::get(),200);
    }

    public function getById($id){
        $predemande = Predemande::find($id);
        if (is_null($predemande)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($predemande,200);
        }
       
    }

    public function update($id,Request $request){
        $predemande = Predemande::find($id);
        if (is_null($predemande)){
            return response()->json("Record Not Found",404); 
        } else{
            $predemande->update($request->all());
            return response()->json($predemande,200);
        }

        
    }

    public function delete($id){
        $predemande = Predemande::find($id);
        if (is_null($predemande)){
            return response()->json("Record Not Found",404); 
        } else{
            $predemande->delete();
            return response()->json(null,204);  
        }
         
    }



}
