<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Conversation;
use App\Flight;
use App\FlightMember;
use App\Message;
use App\Notif;
use Illuminate\Support\Str;
use DB;
Use \Carbon\Carbon;


class ConversationController extends Controller
{

    public function UserConversation(Request $request){
        $ur_id = $request->ur_id;
        $convesrsation = Conversation::where(function ($query) use ($ur_id) {
                                    $query->where('fromid', '=', $ur_id)->Where('fromtype', '=', 'USR');
                                })->orWhere(function($query) use ($ur_id) {
                                    $query->where('toid', '=', $ur_id)->where('totype', '=', 'USR');	
                                }) ->get();

        if (is_null($convesrsation)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($convesrsation,200);
        }
    }


    public function opConversation(Request $request){
        $op_id = $request->op_id;
        $convesrsation =  Conversation::where(function ($query) use ($op_id) {
                                        $query->where('fromid', '=', $op_id)->Where('fromtype', '=', 'OPR');
                                    })->orWhere(function($query) use ($op_id) {
                                        $query->where('toid', '=', $op_id)->where('totype', '=', 'OPR');	
                                    }) ->get();

        if (is_null($convesrsation)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($convesrsation,200);
        }
    }
   
    public function groupConversation(Request $request){
        $convesrsation = DB::table('flights')->join('flight_members', 'flights.id', '=', 'flight_members.id_flight')
                            ->join("conversations",function($join){
                                $join->on("conversations.fromid","=","flights.id");
                            })
                            ->select('conversations.id as id','flight_members.id_client as ur_id','name','fromid','fromtype','toid','totype') 
                            ->where('conversations.fromtype','=','GRP')
                            ->where('flight_members.id_client','=',$request->ur_id)
                            ->distinct()
                            ->get();

        if (is_null($convesrsation)){
        return response()->json("Record Not Found",404); 
        } else{
        return response()->json($convesrsation,200);
        }

    }

    public function getUserAvatar(Request $request){
       
        $userinfo =  DB::table('userinfos')->select('ur_id','logo') 
                                           ->whereIn('ur_id', $request->listUSR)
                                           ->get(); 
            if (is_null($userinfo)){
                return response()->json("Record Not Found",404); 
            } else{
                return response()->json($userinfo,200);
            }
    }

    public function getOpAvatar(Request $request){
       
        $opinfo =  DB::table('opinfos')->select('op_id','logo') 
                                           ->whereIn('op_id', $request->listOPR)
                                           ->get(); 
            if (is_null($opinfo)){
                return response()->json("Record Not Found",404); 
            } else{
                return response()->json($opinfo,200);
            }
    }
    
    public function unReadChatNotifs(Request $request){
       
        $totMessages = DB::table('notifs')
                        ->select('context_id',DB::raw('count(*) as total'))
                        ->groupBy('context_id')
                        ->where('context','=','CHAT')
                        ->where('src','=',$request->src)
                        ->where('src_id','=',$request->src_id)
                        ->where('rread','=','0')
                        ->get();

        if (is_null($totMessages)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($totMessages,200);
        }
    }

    public function createIfNotExist(Request $request){
        
        $fromId = $request->fromId;
        $toId = $request->toId;
        $toType = $request->toType;
        $fromType = $request->fromType;
        $name = $request->name;
        
        $conversation = Conversation::whereIn('fromid',[$fromId,$toId])
                                    ->whereIn('toid',[$fromId,$toId])
                                    ->whereIn('fromtype',[$fromType,$toType])
                                    ->whereIn('totype',[$fromType,$toType])
                                    ->get();
        if ($conversation->isEmpty()){

            $data = array(
                'name'=> $name, 
                'fromid'=> $fromId,
                'fromtype'=> $fromType, 
                'toid'=> $toId,
                'totype'=>$toType,
            );
            
           $convId = DB::table('conversations')->insertGetId($data);
           $newConversation = DB::table('conversations')->select('*')->where('id','=',$convId)->get();

           return response()->json($newConversation,200);

        } else{

            return response()->json($conversation,200);
        }

        
    }
    
    public function massUpdateNotif(Request $request){

        $updated = DB::table('notifs')
              ->where('context','=', $request->context)
              ->where('context_id','=', $request->context_id)
              ->where('src_id','=', $request->src_id)
              ->update(['rread' => 1]);
              if (is_null($updated)){
                return response()->json("Record Not Found",404); 
            } else{
                return response()->json($updated,200);
            }
    }
 
    public function getAll(){
        return response()->json(Conversation::get(),200);
    }

    
    public function getById($id){
        $conversation = Conversation::find($id);
        if (is_null($conversation)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($conversation,200);
        }
       
    }


    public function addNew(Request $request){
        $conversation = Conversation::create($request->all());
        return response()->json($conversation,201);
    }

    public function update($id,Request $request){
        $conversation = Conversation::find($id);
        if (is_null($conversation)){
            return response()->json("Record Not Found",404); 
        } else{
            $conversation->update($request->all());
            return response()->json($conversation,200);
        }

        
    }

    public function delete($id){
        $conversation = Conversation::find($id);
        if (is_null($conversation)){
            return response()->json("Record Not Found",404); 
        } else{
            $conversation->delete();
            DB::table('messages')->where('conversation_id', '=', $id)
                                ->delete();
            return response()->json(null,204);  
        }

         
}

}


