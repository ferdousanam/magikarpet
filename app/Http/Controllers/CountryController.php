<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;

class CountryController extends Controller
{
    public function getAll(){
        return response()->json(Country::get(),200);
    }

    
    public function getById($id){
        $country = Country::find($id);
        if (is_null($country)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($country,200);
        }
       
    }

    public function getByCode(Request $request){
       
        $country =  Country::where('code', '=', $request->code)->get(); // crew = 1, staff = 0
            if (is_null($country)){
                return response()->json("Record Not Found",404); 
            } else{
                return response()->json($country,200);
            }
    }

    public function addNew(Request $request){
        $country = Country::create($request->all());
        return response()->json($country,201);
    }

    public function update($id,Request $request){
        $country = Country::find($id);
        if (is_null($country)){
            return response()->json("Record Not Found",404); 
        } else{
            $country->update($request->all());
            return response()->json($country,200);
        }

        
    }

    public function delete($id){
        $country = Country::find($id);
        if (is_null($country)){
            return response()->json("Record Not Found",404); 
        } else{
            $country->delete();
            return response()->json(null,204);  
        }
         
    }
}
