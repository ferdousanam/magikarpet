<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Demandeop;
use App\Demande;
use DB;


class DemandeopController extends Controller
{
    public function getByFilter(Request $request){
       
        $demandeop =  DB::table('demandes')
                        ->join('demandeops', 'demandes.id', '=', 'demandeops.demande_id')
                        ->select('demandeops.id as demandeop_id','demandes.predemande_id as predemande_id','demande_id','op_id','ur_id','flight_type','from_city','to_city','flexibility_distance','ladate','depart_Time',
                        'felxibility_time','model','demandes.seats as seats', 'validity','demandeops.status as status')
                        ->where('op_id', '=', $request->op_id)
                        ->where('demandeops.status', '=', $request->status)
                        ->orderBy('predemande_id','desc')
                        ->get();
        if (is_null($demandeop)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($demandeop,200);
        }

    }
    
    public function getAll(){
        return response()->json(Demandeop::get(),200);
    }

    public function getById($id){
        $demandeop = Demandeop::find($id);
        if (is_null($demandeop)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($demandeop,200);
        }
       
    }

    public function addNew(Request $request){
        $demandeop = Demandeop::create($request->all());
        return response()->json($demandeop,201);
    }

    public function updateStatus(Request $request){
        
        DB::table('demandeops')->whereIn('id', [$request->demandeop_id1,$request->demandeop_id2, $request->demandeop_id3,$request->demandeop_id4,$request->demandeop_id5])
                               ->update(['status' => $request->status]);
    
    }

    public function delete($id){
        $demandeop = Demandeop::find($id);
        if (is_null($demandeop)){
            return response()->json("Record Not Found",404); 
        } else{
            $demandeop->delete();
            return response()->json(null,204);  
        }
         
    }

}
