<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Baseop;

class BaseopController extends Controller
{

    
    public function getByFilter(Request $request){
       
        $baseop =  Baseop::where('op_id', '=', $request->op_id)->get(); // crew = 1, staff = 0
            if (is_null($baseop)){
                return response()->json("Record Not Found",404); 
            } else{
                return response()->json($baseop,200);
            }
    }
    
    public function getAll(){
        return response()->json(Baseop::get(),200);
    }

    public function getById($id){
        $baseop = Baseop::find($id);
        if (is_null($baseop)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($baseop,200);
        }
       
    }

    public function addNew(Request $request){
        $baseop = Baseop::create($request->all());
        return response()->json($baseop,201);
    }

    public function update($id,Request $request){
        $baseop = Baseop::find($id);
        if (is_null($baseop)){
            return response()->json("Record Not Found",404); 
        } else{
            $baseop->update($request->all());
            return response()->json($baseop,200);
        }

        
    }

    public function delete($id){
        $baseop = Baseop::find($id);
        if (is_null($baseop)){
            return response()->json("Record Not Found",404); 
        } else{
            $baseop->delete();
            return response()->json(null,204);  
        }
         
    }



}
