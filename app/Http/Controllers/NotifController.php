<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use \Carbon\Carbon;
use App\Notif;
use App\Baseop;
use App\Demandeop;
use DB;

class NotifController extends Controller
{
    public function NotifyOpDemande(Request $request){
        $from_city = $request->from_city;
        $demande_id = $request->demande_id;

        $baseops =  Baseop::select('op_id')->distinct()->where('airport_code', '=', $from_city)
                         ->orWhere('city_code', '=', $from_city)
                         ->get();

        if (is_null($baseops)){
            
        } else{
            
            foreach($baseops as $baseop){
                //insert demande for OP
                $data = array(
                    'demande_id'=> $demande_id, 
                    'op_id'=> $baseop->op_id,
                    'status'=>'OPEN',
                   
                );
                
                DB::table('demandeops')->insert($data);

                //insert notif for OP
                $data2 = array(
                    'src'=> 'OPR', 
                    'context'=> 'PLAN', 
                    'context_id'=> $demande_id, 
                    'src_id'=> $baseop->op_id,
                    'label'=>'New Flight Request',
                    'ladate'=>Carbon::now()->format('Y-m-d H:i:s'),
                   
                );
                
                DB::table('notifs')->insert($data2);
            }// end foreach
            
            return response()->json("OK",200);
           
        }//end else

    }// end function

    public function getByFilter(Request $request){
       
        $notif =  Notif::where('src_id', '=', $request->src_id)
                        ->where('src', '=', $request->src)
                        ->where('rread', '=', $request->rread)
                        ->orderBy('id','desc')
                        ->get(); // for = OPR/USR
            if (is_null($notif)){
                return response()->json("Record Not Found",404); 
            } else{
                return response()->json($notif,200);
            }
    }
    
    public function getAll(){
        return response()->json(Notif::get(),200);
    }

    public function getById($id){
        $notif = Notif::find($id);
        if (is_null($notif)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($notif,200);
        }
       
    }

    public function addNew(Request $request){
        $notif = Notif::create($request->all());
        return response()->json($notif,201);
    }

    public function update($id,Request $request){
        $notif = Notif::find($id);
        if (is_null($notif)){
            return response()->json("Record Not Found",404); 
        } else{
            $notif->update($request->all());
            return response()->json($notif,200);
        }

        
    }

    public function delete($id){
        $notif = Notif::find($id);
        if (is_null($notif)){
            return response()->json("Record Not Found",404); 
        } else{
            $notif->delete();
            return response()->json(null,204);  
        }
         
    }




   
}
