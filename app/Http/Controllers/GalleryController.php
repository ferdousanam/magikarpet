<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gallery;
use Illuminate\Support\Str;
use DB;

class GalleryController extends Controller
{


    public function upload(Request $request){
        
        $fileName = $request->file('image')->getClientOriginalName();
        $length = Str::of($fileName)->length();
        if($length>24) {
             $fileName = Str::substr($fileName, 0, 9) . Str::substr($fileName, -5, 5);
            }  //for file extension
        $destFile = Str::random(26);
        $destFile = $destFile . $fileName;
        $request->file('image')->move(public_path('/images'),$destFile);
        return response()->json(['url'=>$destFile],200);
    }

    public function download(Request $request){

        $filePath = public_path('images/'. $request->image);
        return response()->download($filePath,'image');
        
    }

    
    public function addMany(Request $request){
        
        $context_id = $request->context_id;
        $context_name = $request->context_name;

        DB::table('galleries')->where('context_id', '=', $context_id)->where('context_name', '=', $context_name)->delete();

        if(Str::of($request->url1)->length() > 0){
            DB::table('galleries')->insert([
                ['context_id' => $context_id, 'context_name' =>$context_name, 'image_link' => $request->url1],
            ]);
        }

        if(Str::of($request->url2)->length() > 0){
            DB::table('galleries')->insert([
                ['context_id' => $context_id, 'context_name' =>$context_name, 'image_link' => $request->url2],
            ]);
        }

        if(Str::of($request->url3)->length() > 0){
            DB::table('galleries')->insert([
                ['context_id' => $context_id, 'context_name' =>$context_name, 'image_link' => $request->url3],
            ]);
        }

        if(Str::of($request->url4)->length() > 0){
            DB::table('galleries')->insert([
                ['context_id' => $context_id, 'context_name' =>$context_name, 'image_link' => $request->url4],
            ]);
        }

        if(Str::of($request->url5)->length() > 0){
            DB::table('galleries')->insert([
                ['context_id' => $context_id, 'context_name' =>$context_name, 'image_link' => $request->url5],
            ]);
        }

        if(Str::of($request->url6)->length() > 0){
            DB::table('galleries')->insert([
                ['context_id' => $context_id, 'context_name' =>$context_name, 'image_link' => $request->url6],
            ]);
        }
        
        return response()->json("OK",201);
    }

    public function getByFilter(Request $request){
       
        $gallery =  Gallery::where('context_id', '=', $request->context_id)->where('context_name', '=', $request->context_name)->orderBy('id', 'asc')->get(); // crew = 1, staff = 0
            if (is_null($gallery)){
                return response()->json("Record Not Found",404); 
            } else{
                return response()->json($gallery,200);
            }
    }

    public function getById($id){
        $gallery = Gallery::find($id);
        if (is_null($gallery)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($gallery,200);
        }
       
    }

    public function addNew(Request $request){
        $gallery = Gallery::create($request->all());
        return response()->json($gallery,201);
    }

    public function update($id,Request $request){
        $gallery = Gallery::find($id);
        if (is_null($gallery)){
            return response()->json("Record Not Found",404); 
        } else{
            $gallery->update($request->all());
            return response()->json($gallery,200);
        }

        
    }

    public function delete($id){
        $gallery = Gallery::find($id);
        if (is_null($gallery)){
            return response()->json("Record Not Found",404); 
        } else{
            $gallery->delete();
            return response()->json(null,204);  
        }
         
    }
}
