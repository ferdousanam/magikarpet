<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;
use App\Conversation;
use Illuminate\Support\Str;
use DB;
Use \Carbon\Carbon;

class MessageController extends Controller
{
    
    
    public function getByFilter(Request $request){
       
        $message =  Message::where('conversation_id', '=', $request->conversation_id)
                            ->where('id', '>=', $request->last_id) // to fetch only new messages
                            ->orderBy('created_at','asc')
                            ->get(); // crew = 1, staff = 0
            if (is_null($message)){
                return response()->json("Record Not Found",404); 
            } else{
                return response()->json($message,200);
            }
    }

    public function getAll(){
        return response()->json(Message::get(),200);
    }

    
    public function getById($id){
        $message = Message::find($id);
        if (is_null($message)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($message,200);
        }
       
    }


    public function addNew(Request $request){

        $message = Message::create($request->all());

        $conversation = Conversation::find($message->conversation_id);
        if (is_null($conversation)){
            return response()->json("Error Notification",404); 
        }

        $sender = $message->sender;
        $type = $message->type;
        $fromtype = $conversation->fromtype;
        $totype = $conversation->totype;
        $fromid = $conversation->fromid;
        $toid = $conversation->toid;


        if($fromid == $sender && $type == $fromtype){
            $data = array(
                'src'=> $totype, 
                'src_id'=> $toid,
                'context'=> 'CHAT', 
                'context_id'=> $conversation->id, 
                'label'=>'New Message Received',
                'ladate'=>Carbon::now()->format('Y-m-d H:i:s'),
                'rread'=>0,
            );
            
            DB::table('notifs')->insert($data);
        }
       
        if($toid == $sender && $type == $totype){
            $data = array(
                'src'=> $fromtype, 
                'src_id'=> $fromid,
                'context'=> 'CHAT', 
                'context_id'=> $conversation->id, 
                'label'=>'New Message Received',
                'ladate'=>Carbon::now()->format('Y-m-d H:i:s'),
                'rread'=>0,
            );
            
            DB::table('notifs')->insert($data);
        }

        if($fromtype == 'GRP') { //notify group

            $receivers = DB::table('flights')->join('flight_members', 'flights.id', '=', 'flight_members.id_flight')
                                ->join("conversations",function($join){
                                    $join->on("conversations.fromid","=","flights.id");
                                })
                                ->select('conversations.id as id','flight_members.id_client as ur_id') 
                                ->where('conversations.fromtype','=','GRP')
                                ->where('conversations.id','=',$conversation->id)
                                ->distinct()
                                ->get();

            if (is_null($receivers)){
                return response()->json("Record Not Found",404); 
            }

            foreach($receivers as $receiver){
                if ($receiver->ur_id != $sender){ // notify all except sender

                    $data = array(
                        'src'=> 'USR', 
                        'src_id'=> $receiver->ur_id,
                        'context'=> 'CHAT', 
                        'context_id'=> $conversation->id, 
                        'label'=>'New Message Received',
                        'ladate'=>Carbon::now()->format('Y-m-d H:i:s'),
                        'rread'=>0,
                    );
                    
                    DB::table('notifs')->insert($data);

                }
            }
            
        }

        

        return response()->json($message,201);
        
    }

    public function update($id,Request $request){
        $message = Message::find($id);
        if (is_null($message)){
            return response()->json("Record Not Found",404); 
        } else{
            $message->update($request->all());
            return response()->json($message,200);
        }

        
    }

    public function delete($id){
        $message = Message::find($id);
        if (is_null($message)){
            return response()->json("Record Not Found",404); 
        } else{
            $message->delete();
            return response()->json(null,204);  
        }
         
}


}
