<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OpInfo;

class OpinfoController extends Controller
{
    public function getByFilter(Request $request){
       
        $opInfo =  OpInfo::where('op_id', '=', $request->op_id)->get(); // crew = 1, staff = 0
            if (is_null($opInfo)){
                return response()->json("Record Not Found",404); 
            } else{
                return response()->json($opInfo,200);
            }
    }

    public function getAll(){
        return response()->json(OpInfo::get(),200);
    }

    public function getById($id){
        $opInfo = OpInfo::find($id);
        if (is_null($opInfo)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($opInfo,200);
        }
       
    }

    public function addNew(Request $request){
        $opInfo = OpInfo::create($request->all());
        return response()->json($opInfo,201);
    }

    public function update($id,Request $request){
        $opInfo = OpInfo::find($id);
        if (is_null($opInfo)){
            return response()->json("Record Not Found",404); 
        } else{
            $opInfo->update($request->all());
            return response()->json($opInfo,200);
        }

        
    }

    public function delete($id){
        $opInfo = OpInfo::find($id);
        if (is_null($opInfo)){
            return response()->json("Record Not Found",404); 
        } else{
            $opInfo->delete();
            return response()->json(null,204);  
        }
         
    }



}
