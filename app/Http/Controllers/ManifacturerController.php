<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Manifacturer;

class ManifacturerController extends Controller
{
    public function getAll(){
        return response()->json(Manifacturer::get(),200);
    }

    public function getById($id){
        $manifacturer = Manifacturer::find($id);
        if (is_null($manifacturer)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($manifacturer,200);
        }
       
    }

    public function addNew(Request $request){
        $manifacturer = Manifacturer::create($request->all());
        return response()->json($manifacturer,201);
    }

    public function update($id,Request $request){
        $manifacturer = Manifacturer::find($id);
        if (is_null($manifacturer)){
            return response()->json("Record Not Found",404); 
        } else{
            $manifacturer->update($request->all());
            return response()->json($manifacturer,200);
        }

        
    }

    public function delete($id){
        $manifacturer = Manifacturer::find($id);
        if (is_null($manifacturer)){
            return response()->json("Record Not Found",404); 
        } else{
            $manifacturer->delete();
            return response()->json(null,204);  
        }
         
    }


}
