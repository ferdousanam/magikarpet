<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;

class CityController extends Controller
{
    public function getByFilter(Request $request){
       
        $city =  City::where('country_code', '=', $request->country_code)->get(); // crew = 1, staff = 0
            if (is_null($city)){
                return response()->json("Record Not Found",404); 
            } else{
                return response()->json($city,200);
            }
    }

    public function getByCode(Request $request){
       
        $city =  City::where('code', '=', $request->code)->get(); // crew = 1, staff = 0
            if (is_null($city)){
                return response()->json("Record Not Found",404); 
            } else{
                return response()->json($city,200);
            }
    }

    public function getAll(){
        return response()->json(City::get(),200);
    }

    public function getById($id){
        $city = City::find($id);
        if (is_null($city)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($city,200);
        }
       
    }

    public function addNew(Request $request){
        $city = City::create($request->all());
        return response()->json($city,201);
    }

    public function update($id,Request $request){
        $city = City::find($id);
        if (is_null($city)){
            return response()->json("Record Not Found",404); 
        } else{
            $city->update($request->all());
            return response()->json($city,200);
        }

        
    }

    public function delete($id){
        $city = City::find($id);
        if (is_null($city)){
            return response()->json("Record Not Found",404); 
        } else{
            $city->delete();
            return response()->json(null,204);  
        }
         
    }


}
