<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Demande;
Use \Carbon\Carbon;
use App\Notif;
use App\Baseop;
use App\Demandeop;
use DB;


class DemandeController extends Controller
{

    public function getByFilter(Request $request){
       
        $demande =  Demande::where('ur_id', '=', $request->ur_id)->where('status', '=', $request->status)->get(); // crew = 1, staff = 0
            if (is_null($demande)){
                return response()->json("Record Not Found",404); 
            } else{
                return response()->json($demande,200);
            }
    }
    


    public function getAll(){
        return response()->json(Demande::get(),200);
    }

    public function getById($id){
        $demande = Demande::find($id);
        if (is_null($demande)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($demande,200);
        }
       
    }

    
    public function addNew(Request $request){
            $demande = Demande::create($request->all());

            $demande_id = $demande->id;
            $from_city = $demande->from_city;

            $baseops =  Baseop::select('op_id')->distinct()->where('airport_code', '=', $from_city)
                            ->orWhere('city_code', '=', $from_city)
                            ->get();

            if (is_null($baseops)){
                
            } else{
                

                foreach($baseops as $baseop){
                    //insert demande for OP
                    $data = array(
                        'demande_id'=> $demande_id, 
                        'op_id'=> $baseop->op_id,
                        'status'=>'OPEN',
                    
                    );
                    
                    DB::table('demandeops')->insert($data);

                    //insert notif for OP
                    $data2 = array(
                        'src'=> 'OPR', 
                        'src_id'=> $baseop->op_id,
                        'context'=> 'DEMANDE', 
                        'context_id'=> $demande_id, 
                        'label'=>'New Flight Request',
                        'ladate'=>Carbon::now()->format('Y-m-d H:i:s'),
                        'rread'=>0,
                    );
                    
                    DB::table('notifs')->insert($data2);


                }// end foreach
            
            }//end else


       

        return response()->json($demande,201);
    }

    public function update($id,Request $request){
        $demande = Demande::find($id);
        if (is_null($demande)){
            return response()->json("Record Not Found",404); 
        } else{
            $demande->update($request->all());
            return response()->json($demande,200);
        }

        
    }

    public function delete($id){
        $demande = Demande::find($id);
        if (is_null($demande)){
            return response()->json("Record Not Found",404); 
        } else{
            $demande->delete();
            return response()->json(null,204);  
        }
         
    }



}
