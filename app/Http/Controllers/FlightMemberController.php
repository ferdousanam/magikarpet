<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FlightMember;
use App\Flight;
use Illuminate\Support\Str;
use DB;
Use \Carbon\Carbon;

class FlightMemberController extends Controller
{
    
    public function getByFilter(Request $request){
       
        $flightMember =  FlightMember::where('id_flight', '=', $request->id_flight)->get(); // crew = 1, staff = 0
            if (is_null($flightMember)){
                return response()->json("Record Not Found",404); 
            } else{
                return response()->json($flightMember,200);
            }
    }

    public function getAll(){
        return response()->json(FlightMember::get(),200);
    }

    public function getById($id){
        $flight_member = FlightMember::find($id);
        if (is_null($flight_member)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($flight_member,200);
        }
       
    }

    public function addNew(Request $request){
        $flight_member = FlightMember::create($request->all());

        //if full, upfate flight status
        $id_flight = $flight_member->id_flight;
        
        $flight = Flight::find($id_flight);
        if (is_null($flight)){
            return response()->json("Record Not Found",404); 
        } 

        //Notify op for new member booking
        $data = array(
            'src'=> 'OPR', 
            'src_id'=> $flight->op_id,
            'context'=> 'PROGRESS', 
            'context_id'=> $flight->id, 
            'label'=>'New Seat(s) booked',
            'ladate'=>Carbon::now()->format('Y-m-d H:i:s'),
            'rread'=>0,
        );
        
        DB::table('notifs')->insert($data);

        $seats = $flight->seats;
        
        $books = DB::table('flight_members')->where('id_flight','=', $id_flight)->sum('seats');
        if (is_null($books)){
            return response()->json("Record Not Found",404); 
        }

        $avlbl = $seats - $books;
        if($avlbl<=0){ // full flight => update status to wco
            $updated = DB::table('flights')->where('id', '=',$id_flight)
                         ->update(['status' => 'WCO']);
            
           //Notify op for flight is now FULL
            $data = array(
                'src'=> 'OPR', 
                'src_id'=> $flight->op_id,
                'context'=> 'PROGRESS', 
                'context_id'=> $flight->id, 
                'label'=>'Flight is now Full',
                'ladate'=>Carbon::now()->format('Y-m-d H:i:s'),
                'rread'=>0,
            );
            
            DB::table('notifs')->insert($data);

            //Notify users for flight is now FULL

            $members = DB::table('flight_members')->where('id_flight','=', $id_flight)->get();
            if (is_null($members)){
                return response()->json("Record Not Found",404); 
            }
            foreach ($members as $member){
                $data = array(
                    'src'=> 'USR', 
                    'src_id'=> $member->id_client,
                    'context'=> 'PROGRESS', 
                    'context_id'=> $member->id_flight, 
                    'label'=>'Flight Confirmation Required',
                    'ladate'=>Carbon::now()->format('Y-m-d H:i:s'),
                    'rread'=>0,
                );
                
                DB::table('notifs')->insert($data);
            }

        }
       
        return response()->json($flight_member,201);

    }

  
    public function update($id,Request $request){
        
        $flight_member = FlightMember::find($id);
        if (is_null($flight_member)){
            return response()->json("Record Not Found",404); 
        }
       
        $flight_member->update($request->all());

        $id_flight = $flight_member->id_flight;
        if($flight_member->confirmed == 1){
             //insert notif for OP
            $flight = Flight::find($id_flight);
            if (is_null($flight)){
                return response()->json("Record Not Found",404); 
            } 
            $data = array(
                'src'=> 'OPR', 
                'src_id'=> $flight->op_id,
                'context'=> 'PROGRESS', 
                'context_id'=> $flight->id, 
                'label'=>'New Seat(s) Confirmation',
                'ladate'=>Carbon::now()->format('Y-m-d H:i:s'),
                'rread'=>0,
            );
            
            DB::table('notifs')->insert($data);
        }

        $confirmedMembers = DB::table('flights')->join('flight_members', 'flights.id', '=', 'flight_members.id_flight')
                     ->where('flight_members.confirmed','=',0)
                     ->where('flights.id','=',$id_flight)
                     ->get();

        if (is_null($confirmedMembers)){
            return response()->json("Record Not Found",404); 
        } 
        
        if($confirmedMembers->isEmpty()) { //change flight status to confirmed
            $updated = DB::table('flights')->where('id', '=',$id_flight)->update(['status' => 'CON']);

            $flight = Flight::find($id_flight);
            if (is_null($flight)){
                return response()->json("Record Not Found",404); 
            } 
            //notify op flight confirmed = ready
            $data = array(
                'src'=> 'OPR', 
                'src_id'=> $flight->op_id,
                'context'=> 'CONFIRMED', 
                'context_id'=> $flight->id, 
                'label'=>'Flight is now Ready!',
                'ladate'=>Carbon::now()->format('Y-m-d H:i:s'),
                'rread'=>0,
            );
            
            DB::table('notifs')->insert($data);

            $members = DB::table('flight_members')->where('id_flight','=', $id_flight)->get();
            if (is_null($members)){
                return response()->json("Record Not Found",404); 
            }
            foreach ($members as $member){
                $data = array(
                    'src'=> 'USR', 
                    'src_id'=> $member->id_client,
                    'context'=> 'CONFIRMED', 
                    'context_id'=> $member->id_flight, 
                    'label'=>'Flight is Ready!',
                    'ladate'=>Carbon::now()->format('Y-m-d H:i:s'),
                    'rread'=>0,
                );
                
                DB::table('notifs')->insert($data);
            }

        }


        return response()->json($flight_member,200);
        
        
    }



    public function delete($id){
        $flight_member = FlightMember::find($id);
        if (is_null($flight_member)){
            return response()->json("Record Not Found",404); 
        } else{
            $flight_member->delete();
            return response()->json(null,204);  
        }
         
    }


}
