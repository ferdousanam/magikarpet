<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Flight;
use App\FlightMember;
use App\City;
use App\Conversation;
use Illuminate\Support\Str;
use DB;
Use \Carbon\Carbon;

class FlightController extends Controller
{

    public function opFlightProgress(Request $request){

        $flight =  DB::table('flights')->join('opinfos', 'flights.op_id', '=', 'opinfos.op_id')
                        ->select('flights.id as id_flight','flights.seats as seats','flights.op_id as op_id','ladate','ac','ac_category','from_airport_code',
                        'from_city','to_city','to_airport_code','std','sta','price_unit','currency','validity','status','model','variation','src','offer_id','cargo','opinfos.logo as logo')
                         ->where('flights.op_id', '=', $request->op_id)
                         ->whereIn('status',[$request->status1,$request->status2,$request->status3])
                         ->orderBy('ladate','asc')
                         ->get(); // crew = 1, staff = 0
            if (is_null($flight)){
                return response()->json("Record Not Found",404); 
            } else{
                return response()->json($flight,200);
            }

       
    }

    public function userFlightProgress(Request $request){
        $flight = DB::table('flights')->join('flight_members', 'flights.id', '=', 'flight_members.id_flight')
                    ->join("opinfos",function($join){
                        $join->on("opinfos.op_id","=","flights.op_id");
                    })
                    ->select('flights.id as id_flight','flight_members.id as id_member','flights.seats as seats','flights.op_id as op_id','ladate','ac','ac_category','from_airport_code',
                     'from_city','to_city','to_airport_code','std','sta','price_unit','currency','validity','status','model','variation','src','offer_id','cargo','confirmed','opinfos.logo as logo') 
                     ->where('flight_members.id_client','=',$request->ur_id)
                     ->whereIn('status',[$request->status1,$request->status2,$request->status3])
                     ->orderBy('ladate','asc')
                     ->get();

                    
        if (is_null($flight)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($flight,200);
        }
       
    }

    public function lastPools(Request $request){
        $ur_id = $request->ur_id;
        $flight = DB::table('flights')->join('flight_members', 'flights.id', '=', 'flight_members.id_flight')
                     ->select('flights.id','ladate','from_city','to_city') 
                     ->where('status','=','CLO')
                     ->where('id_client','=',$ur_id)
                     ->distinct()
                     ->get();

        if (is_null($flight)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($flight,200);
        }
       
    }

    public function poolMembers(Request $request){
        $members = DB::table('flight_members')->join('users', 'flight_members.id_client', '=', 'users.id')
        ->join("userinfos",function($join){
            $join->on("userinfos.ur_id","=","users.id");
        })
        ->select('ur_id','first','last','phone','adress','zipcode', 'country','city','nationality', 'logo')
        ->where('flight_members.id_flight','=',$request->id_flight)
        ->where('flight_members.id_client','<>',$request->ur_id)
        ->orderBy('ladate','asc')
        ->get();

         if (is_null($members)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($members,200);
        }

    }

    public function seatsByFlight(Request $request){
        $flight = DB::table('flights')->join('flight_members', 'flights.id', '=', 'flight_members.id_flight')
                     ->select('flights.id','flight_members.seats') 
                     ->whereIn('status',[$request->status1,$request->status2,$request->status3])
                     ->get();

        if (is_null($flight)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($flight,200);
        }
       
    }

    public function totalSeats(Request $request){
        $from = $request->from;
        $to = $request->to;
        $validity = $request->validity;
        $status = $request->status;

        $totalSeats = DB::table('flights')->join('flight_members', 'flights.id', '=', 'flight_members.id_flight')
        ->select('flights.id','flight_members.seats')
        ->where(function ($query) use ($from) {
                $query->where('from_airport_code', '=', $from)->orWhere('from_city', '=', $from);
        })
        ->where(function($query) use ($to) {
                $query->where('to_airport_code', '=', $to)->orWhere('to_city', '=', $to);	
            }) 
        ->where('validity', '>', $validity)
        ->where('status', '=', $status)
        ->get();

        if (is_null($totalSeats)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($totalSeats,200);
        }

    }
    
    public function flightByOffer(Request $request){

        $offer_id = $request->offer_id;

        $members = DB::table('flights')->join('flight_members', 'flights.id', '=', 'flight_members.id_flight')
        ->select('flights.id','flight_members.seats') 
        ->where('offer_id', '=', $offer_id)
        ->get();

        if (is_null($members)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($members,200);
        }

    }

    public function seatsDeparture(Request $request){
        $from = $request->from;
        $validity = $request->validity;
        $status = $request->status;

        $totalSeats =  DB::table('flights')->join('flight_members', 'flights.id', '=', 'flight_members.id_flight')
                         ->select('flights.id','flight_members.seats')
                         ->whereIn('from_city', $from)
                         ->where('validity', '>', $validity)
                         ->where('status', '=', $status)
                         ->get();

            if (is_null($totalSeats)){
                return response()->json("Record Not Found",404); 
            } else{
                return response()->json($totalSeats,200);
            }
    }

    public function availableDeparture(Request $request){

        $from = $request->from;
        $validity = $request->validity;
        $status = $request->status;

        $flight =  DB::table('flights')->join('opinfos', 'flights.op_id', '=', 'opinfos.op_id')
                    ->whereIn('from_city', $from)
                    ->where('validity', '>', $validity)
                    ->where('status', '=', $status)
                    ->get();

                         

            if (is_null($flight)){
                return response()->json("Record Not Found",404); 
            } else{
                return response()->json($flight,200);
            }
    }

    public function searchFlights(Request $request){

        $from = $request->from;
        $to = $request->to;
        $validity = $request->validity;
        $status = $request->status;

        $flight =  DB::table('flights')->join('opinfos', 'flights.op_id', '=', 'opinfos.op_id')
                    ->where(function ($query) use ($from) {
                        $query->where('from_airport_code', '=', $from)->orWhere('from_city', '=', $from);
                    })->where(function($query) use ($to) {
                        $query->where('to_airport_code', '=', $to)->orWhere('to_city', '=', $to);	
                    }) ->where('validity', '>', $validity)
                        ->where('status', '=', $status)->get();

            if (is_null($flight)){
                return response()->json("Record Not Found",404); 
            } else{
                return response()->json($flight,200);
            }
    }


    public function getByFilter(Request $request){
       
        $flight =  Flight::where('op_id', '=', $request->op_id)
                        ->where('status', '=', $request->status)
                        ->orderBy('id','desc')
                        ->get(); // crew = 1, staff = 0
            if (is_null($flight)){
                return response()->json("Record Not Found",404); 
            } else{
                return response()->json($flight,200);
            }
    }
   
    public function getAll(){
        return response()->json(Flight::get(),200);
    }

    public function timeByZone(Request $request){
        $timeZ = $request->code;
        $tz_obj = new \DateTimeZone($timeZ);
        $today = new \DateTime("now", $tz_obj);
        $myDate = $today->format('Y-m-d');
        $myTime = $today->format('H:i');

        $time= array(
            "time"=>$myTime,
            "date"=>$myDate
        );
        return response()->json($time,200);
    }
    
    public function timeNow(){
        $myTime = Carbon::Now()->format('H:i');
        $myDate = Carbon::today()->format('Y-m-d')  ;
        $time= array(
            "time"=>$myTime,
            "date"=>$myDate
        );
        return response()->json($time,200);
    }


    public function getById($id){
        $flight = Flight::find($id);
        if (is_null($flight)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($flight,200);
        }
       
    }

    public function cityName($code){

        $cities =  City::where('code', '=', $code)->get(); // crew = 1, staff = 0
        if (is_null($cities)){
            return $code;
        } 

        foreach ($cities as $city){
            return $city->name;
        }

    }

    public function openGrpConversation($id_flight){
        
        $flight = Flight::find($id_flight);
        if (is_null($flight)){
            return response()->json("Record Not Found",404); 
        }

        $from_city = $flight->from_city;
        $from_city = $this->cityName($from_city);

        $to_city = $flight->to_city;
        $to_city = $this->cityName($to_city);

        $data = array(
            'name'=> $from_city .' - ' . $to_city, 
            'fromid'=> $id_flight,
            'fromtype'=> 'GRP', 
            'toid'=> $id_flight,
            'totype'=>'GRP',
        );
        
        DB::table('conversations')->insert($data);
         

    }


    public function addNew(Request $request){
        $flight = Flight::create($request->all());
        if($flight->src == 'USR'){ //means flight created from an offer --> notify op
            //insert notif for OP
            $data = array(
                'src'=> 'OPR', 
                'src_id'=> $flight->op_id,
                'context'=> 'FLIGHT', 
                'context_id'=> $flight->id, 
                'label'=>'Offer Accepted, New Flight Created',
                'ladate'=>Carbon::now()->format('Y-m-d H:i:s'),
                'rread'=>0,
            );
            
            DB::table('notifs')->insert($data);
        }

        /// create a conversation groupe
        $this->openGrpConversation($flight->id);

        return response()->json($flight,201);
    }



    public function update($id,Request $request){
        $flight = Flight::find($id);
        if (is_null($flight)){
            return response()->json("Record Not Found",404); 
        } else{
            $flight->update($request->all());

                //delete conversation if flight canceled or closed
                $status = $flight->status;
                $id_flight = $flight->id;

                if ($status=='CAN' || $status=='CLO' ){

                    DB::table('conversations')->where('fromid', '=', $flight->id)
                                            ->where('fromtype', '=', 'GRP')
                                            ->delete();
                
                }

                if ($status=='CON'){
                    //Notify users for flight is now confirmed

                    $members = DB::table('flight_members')->where('id_flight','=', $id_flight)->get();
                    if (is_null($members)){
                        return response()->json("Record Not Found",404); 
                    }
                    foreach ($members as $member){
                    $data = array(
                        'src'=> 'USR', 
                        'src_id'=> $member->id_client,
                        'context'=> 'CONFIRMED', 
                        'context_id'=> $member->id_flight, 
                        'label'=>'Flight is ready',
                        'ladate'=>Carbon::now()->format('Y-m-d H:i:s'),
                        'rread'=>0,
                    );
                    
                    DB::table('notifs')->insert($data);
                   }
                
                }

                
                if ($status=='WCO'){
                    //Notify users for flight is now confirmed

                    $members = DB::table('flight_members')->where('id_flight','=', $id_flight)->get();
                    if (is_null($members)){
                        return response()->json("Record Not Found",404); 
                    }
                    foreach ($members as $member){
                    $data = array(
                        'src'=> 'USR', 
                        'src_id'=> $member->id_client,
                        'context'=> 'PROGRESS', 
                        'context_id'=> $member->id_flight, 
                        'label'=>'Flight is Full',
                        'ladate'=>Carbon::now()->format('Y-m-d H:i:s'),
                        'rread'=>0,
                    );
                    
                    DB::table('notifs')->insert($data);
                   }
                
                }

            return response()->json($flight,200);
        }

        
    }

    public function delete($id){
        $flight = Flight::find($id);
        if (is_null($flight)){
            return response()->json("Record Not Found",404); 
        } else{
            $flight->delete();
            return response()->json(null,204);  
        }
         
    }

    
}
