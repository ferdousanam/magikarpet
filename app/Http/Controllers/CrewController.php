<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Crew;
use Illuminate\Support\Str;

class CrewController extends Controller
{

    public function getByFilter(Request $request){
       
        $crew =  Crew::where('iscrew', '=', $request->iscrew)->where('op_id', '=', $request->op_id)->get(); // crew = 1, staff = 0
            if (is_null($crew)){
                return response()->json("Record Not Found",404); 
            } else{
                return response()->json($crew,200);
            }
    }
    
    public function getAll(){
        return response()->json(Crew::get(),200);
    }

    public function getById($id){
        $crew = Crew::find($id);
        if (is_null($crew)){
            return response()->json("Record Not Found",404); 
        } else{
            return response()->json($crew,200);
        }
       
    }

    public function addNew(Request $request){
        $crew = Crew::create($request->all());
        return response()->json($crew,201);
    }

    public function update($id,Request $request){
        $crew = Crew::find($id);
        if (is_null($crew)){
            return response()->json("Record Not Found",404); 
        } else{
            $crew->update($request->all());
            return response()->json($crew,200);
        }

        
    }

    public function delete($id){
        $crew = Crew::find($id);
        if (is_null($crew)){
            return response()->json("Record Not Found",404); 
        } else{
            $crew->delete();
            return response()->json(null,204);  
        }
         
    }

}
