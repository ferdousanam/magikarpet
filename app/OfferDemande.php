<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfferDemande extends Model
{
    public $timestamps = false;
    protected $table = "offer_demandes";
    public $fillable = ['offer_id','demande_id','predemande_id','ur_id','seats','status','ladate'];
}
