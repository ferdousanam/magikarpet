<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flight extends Model
{
    protected $table = "flights";
    public $fillable = ['op_id','offer_id','flight_type','num','ac','ac_category','from_airport_code','to_airport_code',
    'from_city','to_city','ladate','std','sta','status','cargo','cancel_reason','cancel_comment','seats','price_unit',
    'currency','validity','src','overall','model','variation'];


}
