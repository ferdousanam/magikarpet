<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aircraft extends Model
{
    protected $table = "aircrafts";
    public $fillable = ['op_id','ac','reg','type','category','manif_code','seats','range','speed','intervalle','standup',
                        'cheight','clength','cwidth','luggage_cap','age','nb_engines',
                        'engine1','total_time1','engine2','total_time2','engine3','total_time3'];
                        
}
