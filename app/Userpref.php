<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userpref extends Model
{
    protected $table = "userprefs";
    public $fillable = ['ur_id','direct_flight','stop_overs','aircraft_age','cabin','min_insurance];
}
