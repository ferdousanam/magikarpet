<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userinfo extends Model
{
    protected $table = "userinfos";
    public $fillable = ['ur_id','ladate','phone','adress','zipcode', 'country','city','nationality','logo'];
}
