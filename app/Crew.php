<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Crew extends Model
{
    protected $table = "crews";
    public $fillable = ['op_id','code','first','last','phone','email','image','position','iscrew'];
}
