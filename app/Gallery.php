<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = "galleries";
    public $fillable = ['context_id','context_name','image_link'];
}
