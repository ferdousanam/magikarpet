<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    protected $table = "conversations";
    public $fillable = ['name','fromid','fromtype','toid','totype']; //si group, fromid=toid=id_flight, fromtype=totype=GRP
}

