<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = "cities";
    public $fillable = ['code','iso','country_code','name','timezone','latitude','longitude'];
}
