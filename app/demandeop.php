<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class demandeop extends Model
{
    protected $table = "demandeops";
    public $fillable = ['demande_id','predemande_id','op_id','seats','status']; //status = OPEN, SENT, REJ
}
