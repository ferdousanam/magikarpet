<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Opinfo extends Model
{
    protected $table = "opinfos";
    public $fillable = ['op_id','name','email','adress','zipcode','phone','phone24','phoneceo','phonecustomer','year','ceo',
                        'country','city','aoc','aoc_country','quality_certif','safety_certif','insurance',
                        'website','facebook','linkedin','instagram','twitter','logo'];
}
