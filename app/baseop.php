<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class baseop extends Model
{
    
    protected $table = "baseops";
    public $fillable = ['op_id','city_code','airport_code'];

}
