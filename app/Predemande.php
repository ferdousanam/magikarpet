<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Predemande extends Model
{
    protected $table = "predemandes";
    public $fillable = ['ur_id','flight_type','from_city','to_city','flexibility_distance','ladate', 'depart_Time','felxibility_time',
    'model','seats', 'validity','status','trip','return_flexibility_distance','return_date','return_Time','return_felxibility_time',
    'stopover1','ladate1','depart_Time1',
    'stopover2','ladate2','depart_Time2',
    'stopover3','ladate3','depart_Time3',
    'stopover4','ladate4','depart_Time4',
    'stopover5','ladate5','depart_Time5']; 
}
