<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = "messages";
    public $fillable = ['conversation_id','sender','type','message','rread'];
}
