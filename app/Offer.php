<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $table = "offers";
    public $fillable = ['op_id','demande_id','flight_type','ac','ac_category','from_airport_code','to_airport_code',
    'from_city','to_city','ladate','std','sta','status','seats','price_unit',
    'currency','validity','model','variation'];
}
