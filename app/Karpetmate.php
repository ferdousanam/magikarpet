<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Karpetmate extends Model
{
    protected $table = "karpetmates";
    public $fillable = ['ur_id1','ur_id2'];
}
