<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlightMember extends Model
{
    protected $table = "flight_members";
    public $fillable = ['id_flight','id_client','seats','confirmed'];

}
